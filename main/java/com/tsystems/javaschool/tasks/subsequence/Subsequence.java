package com.tsystems.javaschool.tasks;

import java.util.*;

//Create class subsequence
public class Subsequence {
    public <T> boolean find(List <T> toFind, List <T> whereLookFor) {
        if (toFind.isEmpty() || whereLookFor.isEmpty()) {
            System.out.println("Input is empty");
            System.exit(1);
        }

        int found = 0;
        for(int i=0; i<toFind.size(); i++){
            for(int j=0; j< whereLookFor.size();j++){
                //if element will be found when continuing from next element to avoid collision
                if(toFind.get(i).equals(whereLookFor.get(j))){
                    found++;
                    j++;
                    break;
                }
            }
        }
        //Comparing sum of found elements and "need-to-find" elements
        return (found==toFind.size());
    }
}
