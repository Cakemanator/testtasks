package com.tsystems.javaschool.tasks.subsequence;

import java.util.*;

 class Subsequence {
    public <T> boolean find(List <T> toFind, List <T> whereLookFor) throws IllegalArgumentException {
        boolean isGood = true;
        if(toFind==null || whereLookFor==null) throw new IllegalArgumentException();
        else if (toFind.isEmpty()) {
            return true;
        }
        else if(whereLookFor.isEmpty()) {
            return false;
        }


        int found = 0;
        for(int i=0; i<toFind.size(); i++){
            if (!isGood) break;
            isGood = false;
            for(int j=found; j< whereLookFor.size();j++){
                //if element will be found when continuing from next element to avoid collision
                if(toFind.get(i).equals(whereLookFor.get(j))){
                    isGood = true;
                    found = j+1;
                    break;
                }
            }
        }
        
        return isGood;
    }
}