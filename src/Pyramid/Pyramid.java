
import java.util.*;



class CannotBuildPyramidException extends RuntimeException {
    public CannotBuildPyramidException(String message) {
        super(message);
    }
}


public class Pyramid{

    //All numbers must be valid and should not be repeated
    public static boolean isValid(ArrayList<Integer> inpArr) {
        boolean valid = true;
        for(Integer s : inpArr){
            if ( s == null ){
                throw new CannotBuildPyramidException("Input numbers must not be empty");
            }
        }
        int n = inpArr.size();
        for ( int i = 0; i < n; i++ )
        {
            for ( int j =  i + 1; j < n; j++ )
            {
                if ( inpArr.get(j) == inpArr.get(i) ) throw new CannotBuildPyramidException("Input numbers should not be repeated");
            }
        }
        int j=0;
        do{
            n -= j;
            j++;
        }while(n>0);
        if (n<0)  throw new CannotBuildPyramidException("Invalid amount of input numbers");
    return valid;
    }

    //Method for counting amount of floors for building thi pyramid
    private static int getPyramisHeight(int inpLength){
      int pyrHeight=0;
      do{
          inpLength=inpLength-pyrHeight;
          pyrHeight++;
      }while(inpLength>0);

      return pyrHeight-1;
    }


    public static void buildPyramid(ArrayList<Integer> inpArr){
        //If array is not valid, then throw an exception and exit program
        if(!isValid(inpArr))System.exit(1);
        //we need to know, how many floors can have our pyramid
        int pyrFloors = getPyramisHeight(inpArr.size());
        //Amount of numbers on each floor
        int numsOnFloor = (pyrFloors*2)-1;
        Collections.sort(inpArr);
        //array for building
        int[][] p = new int[pyrFloors][numsOnFloor];


        //Current number for input
        int curNum = inpArr.size()-1;
        //Amount of updated numbers
        int updNums=0;
        //Current floor
        int curFloor = 0;
        //Current position for input
        int curBlock=0;
        //Offset for number input
        int oS = 0;
        //Building the pyramid
        for(curFloor = pyrFloors-1; curFloor>=0; curFloor--){
            curBlock =  numsOnFloor-1;
            while(updNums<(curFloor+1)){
                int num = inpArr.get(curNum);
                p[curFloor][curBlock-oS] = num;
                updNums++;
                curNum--;
                curBlock-=2;
            }
            updNums=0;
            oS++;

        }
        //Printing estimated result
        for(int i=0;i<pyrFloors;i++) {
            System.out.println(Arrays.toString(p[i]));
        }
        }



    public static void main(String[] args){
        //Creating and filling array of numbers
        int[] arrInt = {11, 1, 21, 12, 3, 16, 2, 13, 9, 4, 17, 5, 14, 10, 18, 8, 7, 19, 15, 6, 20};
        ArrayList<Integer> inpArr = new ArrayList<Integer>();
        for(int i =0;i<arrInt.length;i++){
            inpArr.add(arrInt[i]);
        }

        buildPyramid(inpArr);
    }
}