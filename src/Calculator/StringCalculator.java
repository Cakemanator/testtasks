package com.javarush.task.task07.task0716;


import java.io.*;
import java.util.ArrayList;


class Calculator{
    public double result=0;
    //Building decrypted ArrayList for expression
    public static ArrayList<String> expression = new ArrayList<String>();
    public static String estimatedResult;

    public static boolean isNumeric(String str)
    {
        try
        {
            double d = Double.parseDouble(str);
        }
        catch(NumberFormatException nfe)
        {
            return false;
        }
        return true;
    }
    //method for expressions in brackets
    public static void brackets( int beg, int end){
        double result=0.0;
        //Close bracket cannot be earlier than open, exit program in this situation
        if (beg>end){
            System.out.println("null");
            System.exit(1);}
        int opAmount = end - beg;
        String[] br = new String[opAmount];
        for(int i = 0; i<opAmount; i++){
            br[i]=expression.get(beg+i);
        }
        //Array contains numbers in even (including 0) cells and operations in odd
        for(int i = 1; i<br.length; i++){
            if(br[i].equals("*"))result+=Double.parseDouble(br[i-1])*Double.parseDouble(br[i+1]);
            else if(br[i].equals("/"))result+=Double.parseDouble(br[i-1])/Double.parseDouble(br[i+1]);
            else if(br[i].equals("-"))result+=Double.parseDouble(br[i-1])-Double.parseDouble(br[i+1]);
            else if(br[i].equals("+"))result+=Double.parseDouble(br[i-1])+Double.parseDouble(br[i+1]);
            else continue;
        }
        expression.set(beg-1, String.valueOf(result));
        for(int i=beg; i<=end; i++) {
            expression.remove(beg);
        }
    }

    public static void multiply(int mulPos){
        double result = Double.parseDouble(expression.get(mulPos-1))*Double.parseDouble(expression.get(mulPos+1));
        // in  this method only 3 elements can be used, 1 will be resetted, rest 2 will be removed
        expression.set(mulPos-1, String.valueOf(result));
        for(int i=0; i<=1; i++) {
            expression.remove(mulPos);
        }
    }
    public static void divide(int divPos){
        double result = Double.parseDouble(expression.get(divPos-1))/Double.parseDouble(expression.get(divPos+1));
        // in  this method only 3 elements can be used, 1 will be resetted, rest 2 will be removed
        expression.set(divPos-1, String.valueOf(result));
        for(int i=0; i<=1; i++) {
            expression.remove(divPos);
        }
    }
    public static void add(int addPos){
        double result = Double.parseDouble(expression.get(addPos-1))+Double.parseDouble(expression.get(addPos+1));
        // in  this method only 3 elements can be used, 1 will be resetted, rest 2 will be removed
        expression.set(addPos-1, String.valueOf(result));
        for(int i=0; i<=1; i++) {
            expression.remove(addPos);
        }
    }
    public static void subtract(int subPos){
        double result = Double.parseDouble(expression.get(subPos-1))-Double.parseDouble(expression.get(subPos+1));
        // in  this method only 3 elements can be used, 1 will be resetted, rest 2 will be removed
        expression.set(subPos-1, String.valueOf(result));
        for(int i=0; i<=1; i++) {
            expression.remove(subPos);
        }
    }

    //Method for math operations
    public static void exprResult(){
        double result = 0;
        //First of all we need to solve expressions in brackets
        while(expression.size()!=1){
            if(expression.contains("(") & expression.contains(")")) brackets((expression.indexOf("(")+1),(expression.indexOf(")")));
            else if(expression.contains("*")) multiply(expression.indexOf("*"));
            else if(expression.contains("/")) divide(expression.indexOf("/"));
            else if(expression.contains("-")) subtract(expression.indexOf("-"));
            else if(expression.contains("+")) add(expression.indexOf("+"));
        }

    }


    //Firstly we will "decrypt" String expression into lists of operations and numbers
    public static String evaluate(String str){
        //ArrayList for number's sequence
        ArrayList<Double> numbers = new ArrayList<Double>();
        //ArrayList for operation's sequence
        ArrayList<String> operations = new ArrayList<String>();
        String curNum ="";
        //Adding numbers to list
        for(int i = 0; i<str.length();i++){
            int j = 0;
            while((i+j<str.length()) && (isNumeric(Character.toString(str.charAt(i+j))) || Character.toString(str.charAt(i+j)).equals("."))){
                curNum += Character.toString(str.charAt(i+j));
                j++;
            }
            //If was read 1 or more numeric symbols then "i" must move further
            i=(j>=1) ? i+(j-1) : i;
            if(!curNum.equals(""))numbers.add(Double.parseDouble(curNum));
            curNum="";
        }

        //if there is no any numbers, that is not an expression (write "null" and stop program)
        if(numbers.size()==0){
            System.out.println("null");
            System.exit(0);
        }


        //Building operation and specSymbol sequence
        int j = 0;
        for(int i = 0; i<str.length(); i++){

            if(Character.toString(str.charAt(i)).equals("*")) operations.add("*");
            else if(Character.toString(str.charAt(i)).equals("/")) operations.add("/");
            else if(Character.toString(str.charAt(i)).equals("+")) operations.add("+");
            else if(Character.toString(str.charAt(i)).equals("-")) operations.add("-");
            else if(Character.toString(str.charAt(i)).equals("(")) operations.add("(");
            else if(Character.toString(str.charAt(i)).equals(")")) operations.add(")");
                //numbering numbers in the list ("if" operator is to avoid dublicates)
            else if((isNumeric(Character.toString(str.charAt(i))) && ((i-1)<0))
            ||(isNumeric(Character.toString(str.charAt(i))) && !isNumeric(Character.toString(str.charAt(i-1))) && !Character.toString(str.charAt(i-1)).equals("."))){ //Case when first symbol in expression was a digit
                operations.add("num_" + j);
                j++;
            }
            //not processing next 2 cases
            else if(Character.toString(str.charAt(i)).equals(".") || Character.toString(str.charAt(i-1)).equals(".")
                    || (isNumeric(Character.toString(str.charAt(i))) && isNumeric(Character.toString(str.charAt(i-1)))))continue;
                //if there is no any arithmetic operations, that is not an expression (write "null" and stop program)
            else {
                System.out.println("null");
                System.exit(0);
            }

        }
        for(int i = 0; i<operations.size(); i++){
            if(operations.get(i).contains("num")){
                //index of number in numbers list for adding it to expression
                int n = Integer.parseInt(String.valueOf(operations.get(i).charAt(4)));
                expression.add(String.valueOf(numbers.get(n)));
            }
            else expression.add(operations.get(i));
        }
        exprResult();
        estimatedResult = String.format("%.5g%n",Double.parseDouble(expression.get(0)));
        expression.remove(0);
        return estimatedResult;

    }
}
public class StringCalculator{


    public static void main(String[] args) throws IOException{
        Calculator c = new Calculator();
       /*BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
       String str = rd.readLine();
	   */

        try {
            //System.out.println(evaluate(str));
            System.out.println(c.evaluate("10/(2-7+3)*4")); // Result: -20
            System.out.println(c.evaluate("22/3*3.0480")); // Result: 22.352
            System.out.println(c.evaluate("22/4*2.159")); // Result: 11.8745
            System.out.println(c.evaluate("22/4*2,159")); // Result: null
        }
        catch(NullPointerException exc){
            System.out.println("null");

        }
        catch(IllegalArgumentException exc){
            System.out.println("null");
        }
    }
}
public class StringCalculator{


    public static void main(String[] args) throws IOException{
        Calculator c = new Calculator();
       /*BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
       String str = rd.readLine();
	   */

        try {
            //System.out.println(evaluate(str));
            System.out.println(c.evaluate("(1+38)*4-5")); // Result: 151
            System.out.println(c.evaluate("7*6/2+8")); // Result: 29
            System.out.println(c.evaluate("-12)1//(")); // Result: null
        }
        catch(NullPointerException exc){
            System.out.println("null");

        }
        catch(IllegalArgumentException exc){
            System.out.println("null");
        }
    }
}