
import java.util.*;

//Create class subsequence
class Subsequence {
    public <T> boolean find(List <T> toFind, List <T> whereLookFor) {
        if (toFind.isEmpty() || whereLookFor.isEmpty()) {
            System.out.println("Input is empty");
            System.exit(1);
        }

        int found = 0;
        for(int i=0; i<toFind.size(); i++){
            for(int j=0; j< whereLookFor.size();j++){
                //if element will be found when continuing from next element to avoid collision
                if(toFind.get(i).equals(whereLookFor.get(j))){
                    found++;
                    j++;
                    break;
                }
            }
        }
        //Comparing sum of found elements and "need-to-find" elements
        return (found==toFind.size());
    }
}

public class SubsequenceImpl extends Subsequence {

    public static void main(String[] args) {
        Subsequence s = new SubsequenceImpl();
        boolean b = s.find(Arrays.asList(1, 3, 5, 7, 9),
                Arrays.asList(10, 1, 2, 3, 4, 5, 7, 9, 20));
        System.out.println(b);
    }
}
